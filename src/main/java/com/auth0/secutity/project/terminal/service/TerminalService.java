package com.auth0.secutity.project.terminal.service;

import com.auth0.secutity.project.terminal.model.Appointment;
import com.auth0.secutity.project.terminal.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;


@Service
public class TerminalService {

    @Autowired
    private AppointmentRepository terminalRepository;

    public void addTerminal(Appointment terminal) {
        terminalRepository.save(terminal);
    }

    public List<Appointment> getTerminal(){
        return terminalRepository.findAll();
    }

    public void delTerminal(Long id) {
        terminalRepository.deleteById(id);
    }

    public Appointment findOne(Long id) {
        Appointment t = terminalRepository.findById(id).get();

        if(t != null){
            return t;
        }

        return  null;
    }
}
