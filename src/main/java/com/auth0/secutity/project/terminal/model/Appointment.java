package com.auth0.secutity.project.terminal.model;

import com.auth0.secutity.project.user.entity.User;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name = "APPOINTMENT")
public class Appointment {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "STATUS")
    private String status;
    @Column(name = "USER")
    private String user;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "APPOINTMENT")
    private Date appointmentTime;

    @Column(name = "PRESCRIPTION")
    private String prescription;

}
