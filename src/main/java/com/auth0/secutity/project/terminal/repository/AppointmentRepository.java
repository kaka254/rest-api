package com.auth0.secutity.project.terminal.repository;

import com.auth0.secutity.project.terminal.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment,Long> {

}
