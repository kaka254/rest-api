package com.auth0.secutity.project.terminal.controller;

import com.auth0.secutity.project.terminal.model.Appointment;
import com.auth0.secutity.project.terminal.service.TerminalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TerminalController {

    @Autowired
    private TerminalService terminalService;

    @PostMapping("/appointment")
    public ResponseEntity<Object> addAppointment(@RequestBody Appointment appointment){
        Map<String,Object> res = new HashMap<>();
        terminalService.addTerminal(appointment);
        res.put("message","Appointment booked successfully");
        res.put("Appointment",appointment);

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @GetMapping("/appointment")
    public ResponseEntity<Object> getAppointments(){
        Map<String,Object> res = new HashMap<>();
        List<Appointment> appointments = terminalService.getTerminal();

        res.put("message","Appointments fetched successfully");
        res.put("terminals",appointments);

        return new ResponseEntity<>(res,HttpStatus.OK);
    }

    @PutMapping("/appointments")
    public ResponseEntity<Object> updateTerminal(@RequestBody Appointment appointment){
        Map<String,Object> res = new HashMap<>();
        terminalService.addTerminal(appointment);
        res.put("message","Appointments updated successfully");
        res.put("terminals",appointment);

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @DeleteMapping("/appointments/{id}")
    public ResponseEntity<Object> delTerminal(@PathVariable Long id) {
        Map<String,Object> res = new HashMap<>();
        terminalService.delTerminal(id);
        res.put("message","Terminal updated successfully");

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/terminal/{id}")
    public ResponseEntity<Object> getTerminal(@PathVariable Long id ) {
        Map<String,Object> res = new HashMap<>();
        Appointment terminal = terminalService.findOne(id);

        res.put("message","Appointment fetched successfully");
        res.put("terminal",terminal);
        return new ResponseEntity<>(res,HttpStatus.OK);
    }
}
