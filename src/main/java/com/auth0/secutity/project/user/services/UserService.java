package com.auth0.secutity.project.user.services;

import com.auth0.secutity.project.user.model.LoginUser;
import com.auth0.secutity.project.user.model.RegisterUser;

public interface UserService {
    void loginUser(LoginUser loginUser);
    void registerUser(RegisterUser registerUser);
}
