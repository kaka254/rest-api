package com.auth0.secutity.project.user.services;

import com.auth0.secutity.project.user.entity.User;
import com.auth0.secutity.project.user.model.LoginUser;
import com.auth0.secutity.project.user.model.RegisterUser;
import com.auth0.secutity.project.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void loginUser(LoginUser loginUser) {

    }

    @Override
    public void registerUser(RegisterUser registerUser) {
        User user = new User();
        user.setPassword(registerUser.getPassword());
        user.setFullName(registerUser.getFullName());
        user.setEmail(registerUser.getEmail());
        user.setPhoneNumber(registerUser.getPhonenumber());
        user.setCreatedOn(new Date(System.currentTimeMillis()));
        user.setRole(registerUser.getRole());

        userRepository.save(user);
    }
}
