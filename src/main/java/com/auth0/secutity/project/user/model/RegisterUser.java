package com.auth0.secutity.project.user.model;

import lombok.Data;

@Data
public class RegisterUser {
    private String username;
    private String password;
    private String fullName;
    private String email;
    private String phonenumber;
    private String role;
}
