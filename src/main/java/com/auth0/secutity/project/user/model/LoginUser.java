package com.auth0.secutity.project.user.model;

import lombok.Data;

@Data
public class LoginUser {
    private String username;
    private String password;
}
