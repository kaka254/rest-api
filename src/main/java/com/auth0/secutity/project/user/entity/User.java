package com.auth0.secutity.project.user.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "users")
@Data
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "NAME")
    private String fullName;
    @Column(name = "PHONE")
    private String phoneNumber;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "ROLE")
    private String role;
    @Column(name = "CREATED_ON")
    private Date createdOn;
}
