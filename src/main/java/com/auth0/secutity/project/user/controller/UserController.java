package com.auth0.secutity.project.user.controller;

import com.auth0.secutity.project.user.model.RegisterUser;
import com.auth0.secutity.project.user.repository.UserRepository;
import com.auth0.secutity.project.user.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userService;

    //Register user
    @PostMapping("/sign-up")
    public ResponseEntity<Object> register(@RequestBody RegisterUser registerUser){
        registerUser.setPassword(bCryptPasswordEncoder.encode(registerUser.getPassword()));
        userService.registerUser(registerUser);
        Map<String,String> response = new HashMap<>();
        response.put("message","User registered successfully");

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

//    @DeleteMapping("/oauth/token")
//    public void revokeToken(HttpServletRequest request) {
//
//    }
    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){
        Map<String,Object> response = new HashMap<>();
        response.put("message","User fetched successfully");
        response.put("Data",userRepository.findAll());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/users/{role}")
    public ResponseEntity<Object> getUsers(@PathVariable("role") String role){
        Map<String,Object> response = new HashMap<>();
        response.put("message","User fetched successfully");
        response.put("Data",userRepository.findAllByRole(role));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
