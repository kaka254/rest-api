package com.auth0.secutity.project.user.security;

public class SecurityConstant {
    public static final String SECRET ="i_love_working_with_jwt";
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final long EXPIRATION_TIME = (10*24*60*60);
    public static final String SIGN_UP_URL = "/user/sign-up";
}
