DROP TABLE iF EXISTS users;

CREATE TABLE users(
  id BIGINT NOT NULL AUTO_INCREMENT,
  username VARCHAR(30),
  password VARCHAR(150),
  fullName VARCHAR(50),
  email VARCHAR(30),
  createdOn DATE,
  PRIMARY KEY (id));

DROP TABLE iF EXISTS terminal;

CREATE TABLE terminal(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modelNumber VARCHAR(30),
  issuedTo VARCHAR(50),
  contact VARCHAR(30),
  issuedOn DATE,
  PRIMARY KEY (id));